---
title: "Mining of NYC Airbnb data"
author: "Josip Domazet, student at Faculty of Electrical Engineering and Computing, Zagreb (Croatia)"
date: '02 09 2019'
output: 
    html_document:
        toc: true
        theme: cosmo
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

<center><img src="https://images.pexels.com/photos/466685/pexels-photo-466685.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"></center>



# About The Kernel
This kernel focuses on exploratory data analysis, but 
also includes two regression models with _price_ as target variable. <br>
I've focused more on visualisation in this kernel.  <br>
Enjoy! :)


# Loading Packages & Data

```{r message=FALSE}
library(tidyverse)
library(ggthemes)
library(GGally)
library(ggExtra)
library(caret)
library(glmnet)
library(corrplot)
library(leaflet)
library(kableExtra)
library(RColorBrewer)
library(plotly)
th <- theme_fivethirtyeight() + theme(axis.title = element_text(), axis.title.x = element_text()) # global theme for ggplot2 objects
set.seed(252)
airbnb <- read.csv("input/new-york-city-airbnb-open-data/AB_NYC_2019.csv", 
                   encoding="UTF-8", stringsAsFactors = F, na.strings = c(""))
```


# Data exploration

## Structure and features

```{r}
head(airbnb) %>% kable() %>% kable_styling()
```


```{r}
summary(airbnb)
```


Following columns can be ommited since they don't carry any useful information and hence wont' be used in predictive models:

* _id_
* _host_id_.

```{r}
names_to_delete <- c("id", "host_id")
airbnb[names_to_delete] <- NULL
```

Since I used _stringsAsFactors = F_ in read.csv function,
I have to transform following character columns to factor columns:

* _host_name_
* _neighbourhood_group_
* _neighbourhood_
* _room_type_

```{r}
names_to_factor <- c("host_name", "neighbourhood_group", "neighbourhood", "room_type")
airbnb[names_to_factor] <- map(airbnb[names_to_factor], as.factor)

```
Column _ last_review_ has to be converted to _airbnbe_ type using function _ymd_ from lubridate package.


```{r}
airbnb[c("last_review")] <- airbnb[c("last_review")] %>% map(~lubridate::ymd(.x))
```

Sanity check:

```{r}
glimpse(airbnb)
```

Everything looks alright now!

## Missing data

```{r}
missing_airbnb <- airbnb %>% summarise_all(~(sum(is.na(.))/n()))
missing_airbnb <- gather(missing_airbnb, key = "variables", value = "percent_missing")
missing_airbnb <- missing_airbnb[missing_airbnb$percent_missing > 0.0, ] 
ggplot(missing_airbnb, aes(x = reorder(variables, percent_missing), y = percent_missing)) +
geom_bar(stat = "identity", fill = "red", aes(color = I('white')), size = 0.3)+
xlab('variables')+
coord_flip() + 
th  +
  ggtitle("Missing Data") +
  xlab("Column name") +
  ylab("Percentage missing") +
  annotate("text", x = 1.5, y = 0.1,label = "host_name and name have less than 0.001\n percentage missing", color = "slateblue", size = 5)
```

Columns _reviews_per_month_ and _last_review_ have exactly the same value of 
percentage missing (~ 20.56 %). That makes sense because if you don't know when 
the last review was, you can't calculate reviews per month :)

# Data Visualisation

## Price 
The most important (target) variable is _price_.


### Histogram & Density 
```{r}
ggplot(airbnb, aes(price)) +
  geom_histogram(bins = 30, aes(y = ..density..), fill = "purple") + 
  geom_density(alpha = 0.2, fill = "purple") +
  th +
  ggtitle("Distribution of price",
          subtitle = "The distribution is very skewed") +
  theme(axis.title = element_text(), axis.title.x = element_text()) +
  geom_vline(xintercept = round(mean(airbnb$price), 2), size = 2, linetype = 3)
```

### Histogram & Density with log10 transformation

Since the original distribution is very skewed, logarithmic transformation can be used to gain
better insight into data.

```{r message=F, warning=F}
ggplot(airbnb, aes(price)) +
  geom_histogram(bins = 30, aes(y = ..density..), fill = "purple") + 
  geom_density(alpha = 0.2, fill = "purple") +
  th +
  ggtitle("Transformed distribution of price",
          subtitle = expression("With" ~'log'[10] ~ "transformation of x-axis")) +
  #theme(axis.title = element_text(), axis.title.x = element_text()) +
  geom_vline(xintercept = round(mean(airbnb$price), 2), size = 2, linetype = 3) +
  scale_x_log10() +
  annotate("text", x = 1800, y = 0.75,label = paste("Mean price = ", paste0(round(mean(airbnb$price), 2), "$")),
           color = 	"#32CD32", size = 8)
```

### Histogram & Density with log10 transformation for neighbourhood areas


New York City consist of five neighbourhood areas:

1. Manhattan
2. Brooklyn
3. Queens
4. The Bronx
5. Staten Island.

It can be useful to vizualise the distribution of price for every neighbourhood area.

```{r warning=F, error=F}

airbnb_nh <- airbnb %>%
  group_by(neighbourhood_group) %>%
  summarise(price = round(mean(price), 2))


ggplot(airbnb, aes(price)) +
  geom_histogram(bins = 30, aes(y = ..density..), fill = "purple") + 
  geom_density(alpha = 0.2, fill = "purple") +
  th +
  ggtitle("Transformed distribution of price\n by neighbourhood groups",
          subtitle = expression("With" ~'log'[10] ~ "transformation of x-axis")) +
  geom_vline(data = airbnb_nh, aes(xintercept = price), size = 2, linetype = 3) +
  geom_text(data = airbnb_nh,y = 1.5, aes(x = price + 1400, label = paste("Mean  = ",price)), color = "darkgreen", size = 4) +
  facet_wrap(~neighbourhood_group) +
  scale_x_log10() 
```

### Above Average Price Objects by Neighourhood Areas

```{r}
airbnb %>% filter(price >= mean(price)) %>% group_by(neighbourhood_group, room_type) %>% tally %>% 
  ggplot(aes(reorder(neighbourhood_group,desc(n)), n, fill = room_type)) +
  th +
  xlab(NULL) +
  ylab("Number of objects") +
  ggtitle("Number of above average price objects",
          subtitle = "Most of them are entire homes or apartments") +
           geom_bar(stat = "identity")
```

### Boxplot of price by room type

We can also investigate price by room type:

* Entire home or apartment
* Private Room
* Shared Room

```{r warning=F}
ggplot(airbnb, aes(x = room_type, y = price)) +
  geom_boxplot(aes(fill = room_type)) + scale_y_log10() +
  th + 
  xlab("Room type") + 
  ylab("Price") +
  ggtitle("Boxplots of price by room type",
          subtitle = "Entire homes and apartments have the highest avg price") +
  geom_hline(yintercept = mean(airbnb$price), color = "purple", linetype = 2)
```

As expected, entire home or aparment type has the highest average price.
It was also expected that shared rooms would have lower price than private rooms.

### Summary of price distributions
```{r}
airbnb %>% arrange(desc(price)) %>% top_n(10) %>% select(- host_name, -name) %>%  
  ggplot(aes(x = price, fill = neighbourhood_group)) +
  geom_histogram(bins = 50) +
  scale_x_log10() + 
  ggtitle("Summary of price distributions") +
  facet_wrap(~room_type + neighbourhood_group)
```

### Price & Availability 


```{r warning=F}
ggplot(airbnb, aes(availability_365, price)) +
  th +
  geom_point(alpha = 0.2, color = "slateblue") +
  geom_density(stat = "identity", alpha = 0.2) +
  xlab("Availability during year") +
  ylab("Price") +
  ggtitle("Relationship between availability",
          subtitle = "there is not clear relationship") 
```

It's hard to see clear pattern,
but there's a lot of expensive objects 
with few available days and many available days.

### Price & Number Of Reviews
```{r}
ggplot(airbnb, aes(number_of_reviews, price)) +
  th + theme(axis.title = element_text(), axis.title.x = element_text()) +
  geom_point(aes(size = price), alpha = 0.05, color = "slateblue") +
  xlab("Number of reviews") +
  ylab("Price") +
  ggtitle("Relationship between number of reviews",
          subtitle = "The most expensive objects have small number of reviews (or 0)")
```

## Number of objects by neighbourhood areas

```{r}
airbnb %>% group_by(neighbourhood_group) %>% tally() %>% 
  ggplot(aes(x = reorder(neighbourhood_group, n), n)) +
  geom_bar(stat = "identity", fill = "purple") +
  theme_fivethirtyeight() +
  ggtitle("Number of objects by neighbourhood group") +
  geom_text(aes(x = neighbourhood_group, y = 1, label = paste0(n),
                colour = ifelse(neighbourhood_group %in%
                                                              c("Manhattan", "Brooklyn", 
                                                                "Queens"), '1', '2')),
            hjust=-1.5, vjust=.5, size = 4, 
            fontface = 'bold') +
  coord_flip() +
  scale_color_manual(values=c("white","black"), guide = F)
```

### Leaflet map

```{r}
 pal <- colorFactor(palette = c("red", "green", "blue", "purple", "yellow"), domain = airbnb$neighbourhood_group)
 
 leaflet(data = airbnb) %>% addProviderTiles(providers$CartoDB.DarkMatterNoLabels) %>%  addCircleMarkers(~longitude, ~latitude, color = ~pal(neighbourhood_group), weight = 1, radius=1, fillOpacity = 0.1, opacity = 0.1,
                                                                                                        label = paste("Name:", airbnb$name)) %>% 
     addLegend("bottomright", pal = pal, values = ~neighbourhood_group,
     title = "Neighbourhood groups",
     opacity = 1
   )
```

Manhattan has the highest number of objects while it's the smallest neighbourhood group by area.
That can be explained by the fact that it's the most popular neighbourhood group with biggest GDP.

## Correlation Matrix

Spearman correlation will be used since 
I not interested particularly in linear relationships.

```{r}
airbnb_cor <- airbnb[, sapply(airbnb, is.numeric)]
airbnb_cor <- airbnb_cor[complete.cases(airbnb_cor), ]
correlation_matrix <- cor(airbnb_cor, method = "spearman")
corrplot(correlation_matrix, method = "color")
```

# Machine Learning

## Data Splitting

Training set will be 70% percent of the original data.
Objects with price equal to 0 will be ommited since
price can't be 0 (faulty records).
They would make predictive models significantly weaker.

```{r}
airbnb <- airbnb %>% mutate(id = row_number())
airbnb_train <- airbnb %>% sample_frac(.7) %>% filter(price > 0)
airbnb_test  <- anti_join(airbnb, airbnb_train, by = 'id') %>% filter(price > 0)

# sanity check
nrow(airbnb_train) + nrow(airbnb_test) == nrow(airbnb %>% filter(price > 0))
```

## 1st Linear Regression model
```{r}

first_model <- train(price ~ latitude + longitude + room_type + minimum_nights  + availability_365 + neighbourhood_group, data = airbnb_train, method = "lm")
summary(first_model)

```

This model is not so good.
Median residual error is -24.2, while it should be near 0.
\(  R^2 = 0.1 \) is also not so good.

Let's plot the first model.

```{r}
plot(first_model$finalModel)
```

Normal Q-Q plot clearly shows that first linear model **doesn't** satisfy
linear model assumptions (normal Q-Q plot should be straight line).

Since the model seems bad, it will not be used in predicting new prices.

## 2nd Linear Regression Model

Second model will introduce logarithmic transformations.
Also, training dataset will be filtered by price so outliers are removed.

```{r}
learn <- airbnb_train %>% filter(price < quantile(airbnb_train$price, 0.9) & price > quantile(airbnb_train$price, 0.1)) %>% tidyr::drop_na()
second_model <- lm(log(price) ~ room_type + neighbourhood_group + latitude + longitude 
                        + number_of_reviews + availability_365
                       + reviews_per_month + 
                     calculated_host_listings_count + minimum_nights, data = learn)
# Summarize the results
summary(second_model)
```

This model is an improvement.
Median residual error is now -0.0145, which is far better than -25.5 from the first model.
\(  R^2 = 0.491 \) means that this model explains about 50% variance of target variable.

```{r}
plot(second_model)
```

Q-Q plot for this model looks much better then for the previous one.


### Predict prices for training set
```{r}
airbnb_test <- airbnb_test %>% filter(price <= quantile(airbnb_train$price, 0.9) & price >= quantile(airbnb_train$price, 0.1)) %>% tidyr::drop_na()
pred_regression <- predict(second_model, newdata = airbnb_test)
pred_regression <- exp(pred_regression)

RMSE_regression <- sqrt(mean( (airbnb_test$price - pred_regression)**2 ))

SSE <- sum((airbnb_test$price - pred_regression)**2)
SSR <- sum((pred_regression - mean(airbnb_test$price)) ** 2)
R2 <- 1 - SSE/(SSE + SSR)


regression_results <- tibble(
  obs = airbnb_test$price,
  pred = pred_regression,
  diff = pred - obs,
  abs_diff = abs(pred - obs),
  neighbourhood = airbnb_test$neighbourhood,
  name = airbnb_test$name,
  group = airbnb_test$neighbourhood_group,
  type = airbnb_test$room_type
  
)

regression_plot <- regression_results %>% 
  ggplot(aes(obs, pred)) +
geom_point(alpha = 0.1, aes(text = paste("Name:", name, "\nGroup:", group, "\nType:", type,
                                           "\nPrice diff = ", diff))) +
  th +
  scale_x_log10() +
  scale_y_log10() +
  ggtitle("Observed vs predicted",
          subtitle = "Linear regression model") + 
  geom_abline(slope = 1, intercept = 0, color = "blue", linetype = 2)  +
  facet_wrap(~type)

ggplotly(regression_plot)

```

Metrics for testing set:
\( R^2 = 0.43 \) and 
\( RMSE = 41.24 \).

This model could be improved by using _name_ variable and examining which keywords in _name_ indicate higher/lower prices. 


THE END.